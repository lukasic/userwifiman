# -*- coding: utf-8 -*-

from threading import current_thread
from django.contrib.auth.models import User
from libs.netmng_client import NetworkManagerClient

_requests = {}

def get_username():
    t = current_thread()
    if t not in _requests:
         return None
    return _requests[t]


class CurrentUser(object):

	def __init__(self, id=None):
		if not id:
			self.user = User.objects.get(username=get_username())
		else:
			self.user = User.objects.get(id=id)
		self.client = NetworkManagerClient()
		self.devicetypes = None

	def set_password(self, new_password):
		self.client.SetNetworkUserPassword(self.user.id, new_password)

	def get_device_types(self, force=False):
		if self.devicetypes is None or force:
			self.devicetypes = {}	
			data = self.client.GetDeviceTypes()
			for i in data: self.devicetypes[ i['id'] ] = i['name']
			return self.devicetypes
		return self.devicetypes

	def get_devices(self):
		self.get_device_types()

		devices = []
		data = self.client.GetDevices({ 'owner': self.user.id, 'active': True })
		for i in data:
			interfaces = self.client.GetInterfaces({ 'device': i['id'], 'type': self.client.WIFI_INTERFACE_ID })
			devices.append({
				'id': i['id'],
				'name': i['hostname'],
				'type': self.devicetypes[i['type']],
				'mac': ', '.join( inter['mac_address'] for inter in interfaces ),
				'editable': i['type'] in (1, 2, 3)
			})
		return devices

	def create_device(self, type, name):
		self.get_device_types()

		type_id = 1
		for key, value in self.devicetypes.items():
			if value.lower() == type.lower():
				type_id = key

		hostname = "{user}-{name}".format(user=self.user.username, name=name)

		self.client.CreateDevice({
			'owner': self.user.id,
			'active': True,
			'hostname': hostname,
			'type': type_id
		})

	def get_device_by_name(self, device_name):
		devices = self.get_devices()
		device_id = None
		for d in devices:
			if d['name'] == device_name:
				return d
		return None

	def set_device_wifi_mac(self, device_name, mac):
		device_id = self.get_device_by_name(device_name)['id']
		interfaces = self.client.GetInterfaces({ 'device': device_id, 'type': self.client.WIFI_INTERFACE_ID })

		if len(interfaces) == 0:
			data = self.client.CreateInterface({
				'device': device_id,
				'type': self.client.WIFI_INTERFACE_ID,
				'mac_address': mac
			})
			print(data)
		else:
			print(interfaces)
			data = self.client.UpdateInterface(interfaces[0]['id'], { 'mac_address': mac })
			print(data)


