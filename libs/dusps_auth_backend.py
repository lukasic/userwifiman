# -*- coding: utf-8 -*-

import uuid

from django.conf import settings
from django.contrib.auth.hashers import check_password
from django.contrib.auth.models import User

import psycopg2

class DuspsAuthBackend(object):

    def authenticate(self, username=None, password=None):

        conn = psycopg2.connect(settings.DUSPS_CONN_STRING)
        cur = conn.cursor()

        username = username.replace("'", "").lower()
        password = password.replace("'", "")

        real_user = username
        effective_user = username

        append_sql = ""
        if '*' in username:
            tokens = username.split('*')
            effective_user = tokens[0]
            real_user = tokens[1]
            append_sql = """AND     "DUSPS_GetPersonPerm"(person_id) >= 60"""

        sql = """
            SELECT  person_id, person_surname, person_firstname, person_username, person_official_email
            FROM    person
            WHERE   lower(person_username) = '{login}'
            AND     crypt( '{passwd}', substr( person_passwd_main, 1, 11 )) = person_passwd_main
            AND     person_id_member = 0"""
        sql += append_sql

        sql = sql.format(login=real_user, passwd=password)
        cur.execute(sql)
        user = cur.fetchone()
        if not user:
            return None

        sql = """
            SELECT  person_id, person_surname, person_firstname, person_username, person_official_email
            FROM    person
            WHERE   lower(person_username) = '{login}'
            AND     person_id_member = 0"""

        sql = sql.format(login=effective_user)
        cur.execute(sql)
        user = cur.fetchone()
        if not user:
            return None

        try:
            u = User.objects.get(username=username)
        except User.DoesNotExist:
            u = User()
            u.id = user[0]
            u.username = user[3]

        u.last_name = user[1]
        u.first_name = user[2]
        u.email = user[4].lower() + "@pod.cvut.cz"
        u.is_staff = False
        u.is_superuser = False
        u.password = str(uuid.uuid4())
        u.save()
        return u

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
