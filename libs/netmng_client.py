# -*- coding: utf-8 -*-

from userwifiman.settings import NETWORK_MANAGER_REST_URL, NETWORK_MANAGER_AUTH_TOKEN
import urllib.request
from urllib.parse import urlencode
import json
import requests

from requests.auth import AuthBase

class AuthorizationHeaderTokenAuth(AuthBase):
    def __call__(self, r):
        r.headers['Authorization'] = "Token {}".format(NETWORK_MANAGER_AUTH_TOKEN)
        return r


class RestReq(object):

	@staticmethod
	def _auth():
		return AuthorizationHeaderTokenAuth()

	@staticmethod
	def GET(url, query_params={}):
		query = '&'.join( '{}={}'.format(x[0], x[1]) for x in query_params.items() )
		if query: url = url + "?" + query
		return requests.get(url, auth=RestReq._auth()).json()

	@staticmethod
	def POST(url, post_data={}):
		headers = {'content-type': 'application/json'}
		data = requests.post(url, data=json.dumps(post_data), headers=headers, auth=RestReq._auth())
		print(data)
		return data.json()

	@staticmethod
	def PATCH(url, data={}):
		print('PATCH {} {}'.format(url, json.dumps(data)))
		data = requests.patch(url, data, auth=RestReq._auth())
		print(data)
		return data.json()


class NetworkManagerClient(object):

	WIFI_INTERFACE_ID = 4

	def __init__(self):
		self.url = NETWORK_MANAGER_REST_URL

	def GetDevices(self, filter={}):
		url = self.url + "/endpoints/device.json/"
		return RestReq.GET(url, filter)['results']

	def GetInterfaces(self, filter={}):
		url = self.url + "/endpoints/interface.json/"
		return RestReq.GET(url, filter)['results']

	def GetDeviceTypes(self, filter={}):
		url = self.url + "/endpoints/devicetype.json/"
		return RestReq.GET(url, filter)['results']

	def CreateDevice(self, data={}):
		url = self.url + "/endpoints/device.json/"
		return RestReq.POST(url, data)

	def SetNetworkUserPassword(self, user, password):
		url = self.url + "/users/networkusers/{user}.json/".format(user=user)
		return RestReq.PATCH(url, {'password': password})

	def CreateInterface(self, data={}):
		url = self.url + "/endpoints/interface.json/"
		return RestReq.POST(url, data)

	def UpdateInterface(self, interface_id, data={}):
		url = self.url + "/endpoints/interface/{id}.json/".format(id=interface_id)
		return RestReq.PATCH(url, data)
