# -*- coding: utf-8 -*-

from django import forms
import re

class ChangePasswordForm(forms.Form):
    newpassword = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'zadej nové heslo',
        'class': 'u-full-width'
    }), error_messages={'required': 'Heslo nebylo zadáno.'})


DeviceTypes = (
    ('notebook', 'Notebook'),
    ('smartphone', 'Smartphone'),
    ('tablet', 'Tablet'),
)

class DeviceAddForm(forms.Form):
    name = forms.CharField(error_messages={'required': 'Jméno zařízení nebylo zadáno.'})
    type = forms.ChoiceField(choices=DeviceTypes)

    def __init__(self, *args, **kwargs):
        super(DeviceAddForm, self).__init__(*args, **kwargs)
        self.fields['name'].widget.attrs['placeholder'] = "iphone"
        self.fields['name'].widget.attrs['class'] = "u-full-width"
        self.fields['type'].widget.attrs['class'] = "u-full-width"


class ChangeMacAddrForm(forms.Form):
    device = forms.ChoiceField()
    macaddr = forms.CharField(error_messages={'required': 'MAC adresa nebyla zadána.'})

    def clean(self):
        cleaned_data = super(ChangeMacAddrForm, self).clean()
        macaddr = cleaned_data.get('macaddr', '')
        if not macaddr:
            return cleaned_data
        if not re.match('^([a-zA-Z0-9]{2}:){5}[a-zA-Z0-9]{2}$', macaddr):
            raise forms.ValidationError("'{}' není ve formátu aa:bb:cc:dd:ee:ff.".format(macaddr))
        return cleaned_data

    def __init__(self, current_user, *args, **kwargs):
        super(ChangeMacAddrForm, self).__init__(*args, **kwargs)

        choices = []
        for dev in current_user.get_devices():
            choices.append( (dev['name'], dev['name']) )

        self.fields['device'].choices = choices
        self.fields['device'].widget.attrs['class'] = "u-full-width"
        self.fields['macaddr'].widget.attrs['class'] = "u-full-width"
        self.fields['macaddr'].widget.attrs['placeholder'] = "aa:bb:cc:dd:ee:ff"

