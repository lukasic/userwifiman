from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required

from frontend import forms
from libs.netmng_client import NetworkManagerClient
from userwifiman.functions import CurrentUser
from django.contrib import messages

@login_required
def index(request):
	current_user = CurrentUser(request.user.id)
	devices = current_user.get_devices()


	f = {
		'changepwform': forms.ChangePasswordForm,
		'deviceaddform': forms.DeviceAddForm,
		'changemacaddrform': forms.ChangeMacAddrForm(current_user)
	}

	if request.method == 'POST':
		form = None
		if request.POST.get('form') == "changepwform":
			form = forms.ChangePasswordForm(request.POST)
			
			f['changepwform'] = form
			if form.is_valid():
				newpassword = request.POST.get('newpassword')
				print("Setting new password: " + newpassword)
				current_user.set_password(newpassword)
				messages.add_message(request, messages.INFO, 'Heslo bylo změneno..')
				return HttpResponseRedirect("/")

		elif request.POST.get('form') == "deviceaddform":
			form = forms.DeviceAddForm(request.POST)

			f['deviceaddform'] = form
			if form.is_valid():
				data = form.cleaned_data
				print("Adding new device: {0} / {1}-{2} ".format(data['type'], request.user.username, data['name']))
				current_user.create_device(data['type'], data['name'])
				messages.add_message(request, messages.INFO, 'Zařízení bylo vytvořeno.')
				return HttpResponseRedirect("/")

		elif request.POST.get('form') == "changemacaddrform":
			form = forms.ChangeMacAddrForm(current_user, request.POST)

			f['changemacaddrform'] = form
			if form.is_valid():
				data = form.cleaned_data
				print("Setting MAC address: {0} -> {1}".format(data['device'], data['macaddr']))
				current_user.set_device_wifi_mac(data['device'], data['macaddr'])
				messages.add_message(request, messages.INFO, 'MAC adresa byla nastavena.')
				return HttpResponseRedirect("/")

		f['current'] = form


	return render(request, 'index.html', {
		'forms': f,
		'devices': devices,
		'devices_count': len(devices),
		'messages': messages.get_messages(request)
	})

